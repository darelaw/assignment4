/***
 * This is a application for weight average in the class.
 * This application calculate the average of entire tests base on their weight percentage of the class.
 * For example; if text One is 30% of the entire class and i got 85 in that text.
 * This software will calcuate them all.
 * 
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
public class Scores extends JFrame{
    private JLabel  blankoneL, blanktwoL, blankthreeL, 
                   scoreL, weightL, oneL, twoL, threeL,fourL,averageL;
    private JTextField  oneTF, twoTF, threeTF, fourTF,woneTF, wtwoTF,
                        wthreeTF, wfourTF, averageTF;
    private JButton calculateB, exitB;
    private CalculateButtonHandler cbHandler;
    private ExitButtonHandler ebHandler;
    private static final int WIDTH = 600;
    private static final int HEIGHT = 400;
    JFrame pane = new JFrame();
    /**
     * The code below creates eigth labels.
     * One label for each test.
     */
    public Scores() { 
        blankoneL = new JLabel("");
        blanktwoL = new JLabel("");
        blankthreeL = new JLabel("");
        scoreL = new JLabel("SCORE", SwingConstants.CENTER);
        weightL = new JLabel("WEIGHT", SwingConstants.CENTER);
        oneL = new JLabel("TEST NO. 1 ", SwingConstants.RIGHT);
        twoL = new JLabel("TEST NO. 2 ", SwingConstants.RIGHT);
        threeL = new JLabel("TEST NO. 3 ", SwingConstants.RIGHT);
        fourL = new JLabel("TEST NO. 4 ", SwingConstants.RIGHT);
        averageL = new JLabel("Average Score: ", SwingConstants.RIGHT);
        /**
         * The code below creates the all the textfield in panel.
         * Associating values action button to each textfield.
         */
        oneTF = new JTextField(5);
        oneTF.setBackground(Color.PINK);
        twoTF = new JTextField(5);
        twoTF.setBackground(Color.PINK);
        threeTF = new JTextField(5);
        threeTF.setBackground(Color.PINK);
        fourTF = new JTextField(5);
        fourTF.setBackground(Color.PINK);
        woneTF = new JTextField(5);
        woneTF.setBackground(Color.PINK);
        wtwoTF = new JTextField(5);
        wtwoTF.setBackground(Color.PINK);
        wthreeTF = new JTextField(5);
        wthreeTF.setBackground(Color.PINK);
        wfourTF = new JTextField(5);
        wfourTF.setBackground(Color.PINK);
        averageTF = new JTextField(5);
        averageTF.setBackground(Color.WHITE);
        /**
         * Create the calculate button to preform action of the panel
         * Main action = Calculate. 
         */
        setBackground(Color.GREEN);
        calculateB = new JButton("Calculate");
        calculateB.setBackground(Color.YELLOW);
        cbHandler = new CalculateButtonHandler();
        calculateB.addActionListener(cbHandler);
        /**
         * Exit button. 
         * One click, page close. 
         */
        exitB = new JButton("Exit");
        exitB.setBackground(Color.GREEN);
        ebHandler = new ExitButtonHandler();
        exitB.addActionListener(ebHandler);
        /**
         * Window title 
         */
        setTitle("Weighted Average Computation");
         /**
          * Fetching the container
          */
 
        Container pane = getContentPane();
         /**
          * Layout format. 
          */
               
        pane.setLayout(new GridLayout(7,3));
            /**
             * placing all the components in a pane.
             * adding button space to pane for weight average conversion.
             */
        pane.add(blankoneL);
        pane.add(scoreL);
        pane.add(weightL); 
        pane.add(oneL);
        pane.add(oneTF);
        pane.add(woneTF); 
        pane.add(twoL);
        pane.add(twoTF);
        pane.add(wtwoTF);
        pane.add(threeL);
        pane.add(threeTF);
        pane.add(wthreeTF); 
        pane.add(fourL);
        pane.add(fourTF);
        pane.add(wfourTF);
        pane.add(averageL);
        pane.add(averageTF);
        pane.add(blanktwoL); 
        pane.add(calculateB);
        pane.add(blankthreeL);
        pane.add(exitB); 
        /**
         * Size of the window; Height and wide.
         * Visibility is set on.
         * 
         */
        setSize(WIDTH, HEIGHT);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
    }
    private class CalculateButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            double one, wone, two, wtwo,
                    three, wthree, four, wfour, ave;
            DecimalFormat twoDigits = new DecimalFormat("0.00");
            one = Double.parseDouble(oneTF.getText());
            wone = Double.parseDouble(woneTF.getText());
            two = Double.parseDouble(twoTF.getText());
            wtwo = Double.parseDouble(wtwoTF.getText());
            three = Double.parseDouble(threeTF.getText());
            wthree = Double.parseDouble(wthreeTF.getText());
            four = Double.parseDouble(fourTF.getText());
            wfour = Double.parseDouble(wfourTF.getText());
            ave = (one * wone + two * wtwo + three * wthree
                 + four * wfour)/(wone+wtwo+wthree+wfour);
            averageTF.setText(""+ twoDigits.format(ave));
        }
    }
    private class ExitButtonHandler implements ActionListener{
        public void actionPerformed(ActionEvent e){
            System.exit(0);
        }
    }
    public static void main(String[] args) {
        Scores wAveObject = new Scores();
    }
}

