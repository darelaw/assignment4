/*This is class assignment 2
 * It contains variable associate with integers with some Arithmetic calculations.
 * This code prompts the user to enter five numbers.
 * This code sums all the number entered also calculates the average.
 * The sum and average numbers are displayed below.
 */

import java.util.Scanner;
public class Assignment2 {

	public static void main( String [] args) {
	
		System.out.println( 1/ 2);
		System.out.println( 1 % 2);
		System.out.println( 1.0 / 2);
		System.out.println( 5 + 7 / 2);
		System.out.println("Beat" + ' ' + "Army" );
		System.out.println( 6 + 13 / 5 - 35 % 3);
		System.out.println( 3.5 * (5/4));
		System.out.println( (3.5 * 5)/4);
		int n1, n2, n3, n4, n5;
			
		
	System.out.println("Please enter five numbers");
	
		Scanner keyboard = new Scanner (System.in);
	n1= keyboard.nextInt();
	n2= keyboard.nextInt();
	n3= keyboard.nextInt();
	n4= keyboard.nextInt();
	n5= keyboard.nextInt();
	
	System.out.println(n1+n2+n3+n4+n5 + " is the sum of the number entered " );
	System.out.println("The average  number entered is = " + (n1+n2+n3+n4+n5) / 5 );
	
	
	/*
	 * I tried to include the MIN, MAX, MODE, MEDIAN code for the extra credit but all void.
	 * I think you have to have a specific numbers to able to calculate that. 
	 */
	 
	}
}